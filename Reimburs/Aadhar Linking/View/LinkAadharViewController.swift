//
//  LinkAadharViewController.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.

//

import UIKit

class LinkAadharViewController: UIViewController {
    
    var model = LinkAadharModel()

    @IBOutlet weak var aadharNumberTextField: UITextField! {
        didSet {
            aadharNumberTextField.delegate = self
        }
    }
    
    @IBOutlet weak var otpTextField: UITextField! {
        didSet {
            otpTextField.delegate = self
        }
    }
    
    
    @IBOutlet weak var verifyButton: UIButton! {
        didSet {
            verifyButton.isEnabled = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapVerify(_ sender: UIButton) {
        model.shouldLinkAadhar(key: true) { (result) in
            DispatchQueue.main.async {
            if result != nil {
                self.showAlert(message: "Aadhar Linked SuccessFully")
                self.dismiss(animated: true, completion: nil)
            } else{
                self.showAlert(message: "Could not link Aadhar")
            }
        }
    }
    }
    
    
    @IBAction func aadharTextChanged(_ sender: UITextField) {
        model.aadhar = sender.text
        enableVerify(key: shouldEnableVerify)
    }
    
    @IBAction func otpTextChanged(_ sender: UITextField) {
        model.otp = sender.text
        enableVerify(key: shouldEnableVerify)
    }
    
    var shouldEnableVerify :Bool {
        
        if let aadhar = aadharNumberTextField.text , let _ = otpTextField.text , aadhar.count == 12 {
            return true
        }
        return false
        
    }
    
    func enableVerify(key : Bool) {
        verifyButton.isEnabled = key
        verifyButton.backgroundColor = key ? #colorLiteral(red: 0.3308000096, green: 0.7795645622, blue: 0.1121544064, alpha: 1) : #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
    }

    

}

extension LinkAadharViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        dismissKeyboard()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
}



