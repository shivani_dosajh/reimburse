//
//  AadharAuthModel.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.
//

import Foundation
import ObjectMapper


struct AadharModel : Mappable {
    
    var status : String?
    var message : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        status      <- map["kyc_data.status"]
        message     <- map["kyc_data.msg"]
        
    }
    
    
}

