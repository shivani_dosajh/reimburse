//
//  LinkAadharModel.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


class LinkAadharModel {
    
    var aadhar : String!
    var otp : String!
    
    func shouldLinkAadhar(key : Bool, completionHandler: @escaping(AadharModel?) -> Void) {
        if key {
            let params = [
                "aadhaar_id" : aadhar,
                "otp" : otp,
                "txn_id" : aadhar
            ]
            let headers = [
                "Authorization" : "No",
                "Content-type" : "application/json"
              ]
            
            Alamofire.request("https://ext.digio.in:444/test/otpkyc", method: .post, parameters: params, encoding: JSONEncoding.default
                , headers: headers).responseObject { (response: DataResponse<AadharModel>) in
                    switch response.result {
                        
                    case .success(let value): print(value)
                    UserDefaults.standard.set(self.aadhar, forKey: Constants.aadharNum)
                        completionHandler(value)
                        
                    case .failure(let error): print(error)
                        completionHandler(nil)
                        
                    }
            }
        }
    }
    
    
}
