//
//  LoginModel.swift
//
//  Created by Shivani Dosajh 
//  Copyright © 2018 Shivani Dosajh. All rights reserved.
////
import Foundation
import FirebaseAuth
import  FirebaseDatabase

// Data model for login module

protocol LoginModel {
    // Uses Firebase to authenicate user and login
    func loginUser( email : String , password : String , completionHandler : @escaping(Bool, Error?) -> Void)
    
}

// Implementation of the login Module

class LoginModelImplementation : LoginModel {
    
    func loginUser( email : String , password : String , completionHandler : @escaping(Bool, Error?) -> Void) {
        
        DispatchQueue.global().async {
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                
             UserDefaults.standard.set(user?.uid, forKey: Constants.userId )
                  let userRef =  Database.database().reference(withPath: "Users")
                let childRef = userRef.child((user?.uid)!)
                childRef.setValue(user?.displayName)
                
                
              //  childRef.setValue(, forKey: "name")
                childRef.setValue(user?.email)
                childRef.setValue(user?.phoneNumber)
                
                
                            
                if error != nil {
                    completionHandler(false , error)
                } else {
                    completionHandler(true , error)
                }
            }
            
        }
    }
    
}

