//
//  UploadDocsModel.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.

//

import Foundation
import UIKit

class UploadDocsModel {
    
    var docNameArray : [String]! {
        didSet {
            docImagesArray = Array<String>(repeating: "", count: docNameArray.count)
            uploadCountArray = Array<Int>(repeating: -1, count: docNameArray.count)
            
        }
    }
    var docImagesArray : [String?]!
    var uploadCountArray : [Int]!
}
