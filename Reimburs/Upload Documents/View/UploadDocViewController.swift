//
//  UploadDocViewController.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.

//

import UIKit
import FirebaseStorage

class UploadDocViewController: UIViewController {

    var model : UploadDocsModel!
    var imagePicker = UIImagePickerController()
  
    var currentRowIndex: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
    }

 
    @IBOutlet weak var docTableView: UITableView! {
        didSet {
            docTableView.delegate = self
            docTableView.dataSource = self
            docTableView.register(UINib(nibName: "DocUploadTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.docUploadReuseId)
        }
    }
    
    @IBOutlet weak var verifyButton: UIButton!
    
    @IBAction func submitForVerification(_ sender: UIButton) {
    }
    
    
    @objc func addImage() {
                let alertController = UIAlertController(title: "Digi Locker", message: "How do you want to upload report?", preferredStyle: .alert)
                let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
                    self.imagePicker.sourceType = .camera
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                let albumAction = UIAlertAction(title: "Photo Album", style: .default) { (action) in
                    self.imagePicker.sourceType = .photoLibrary
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
                let dismissAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alertController.addAction(cameraAction)
                alertController.addAction(albumAction)
                alertController.addAction(dismissAction)
                present(alertController, animated: true, completion: nil)
    }

}

extension UploadDocViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.docNameArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.docUploadReuseId) as? DocUploadTableViewCell else {
            return UITableViewCell()
        }
        
        cell.loadCell(name: model?.docNameArray?[indexPath.row] ?? "", count: model?.uploadCountArray[indexPath.row] ?? -1 )
        
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //ToDO: Add code for uploading a file
        currentRowIndex = indexPath.row
        addImage()
        
    }
    
    
}

extension UploadDocViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        // Todo :
        imagePicker.showLoadingView()
        uploadToServer(image: image){ (imagePath) in
            self.model?.docImagesArray[self.currentRowIndex] = imagePath
            DispatchQueue.main.async {
                self.docTableView.reloadData()
                self.imagePicker.hideLoadingView()
                self.imagePicker.dismiss(animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    
    func uploadToServer(image: UIImage?, completionHandler: @escaping(String) -> Void ){
        
        
        // Create a root reference
        let storageRef = Storage.storage().reference().child("newClaim2.png")
        
        if let imageData = UIImagePNGRepresentation(image!) {
            storageRef.putData(imageData, metadata: nil) { (metadata, error) in
                if error != nil {
                    print(error!)
                    self.model?.uploadCountArray[self.currentRowIndex] = -1
                    return
                }
                print(metadata!)
                let path = "\(storageRef.parent()!)\\(metadata!.name!)"
                self.model?.uploadCountArray[self.currentRowIndex] = 1
                
                
                print(path)
                completionHandler(path)
                
            }
        }
        
    }
    
}
