//
//  DocUploadTableViewCell.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.


//

import UIKit

class DocUploadTableViewCell: UITableViewCell {
    var delegate : UIViewController?

    @IBOutlet weak var documentImageView: UIImageView!
  
    @IBOutlet weak var docNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func loadCell(name: String, count: Int) {
        docNameLabel.text = name
        switch count {
        case -1 : documentImageView.image = #imageLiteral(resourceName: "attach")
        case 0 : documentImageView.image = #imageLiteral(resourceName: "no")
        case 1 : documentImageView.image = #imageLiteral(resourceName: "yes")
        default: return
        }
        
        
        
    }
    
}
