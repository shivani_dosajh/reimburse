//
//  SchemeInfoViewController.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.
//

import UIKit

class SchemeInfoViewController: UIViewController {
    
    var model : schemeInfo!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var infoButton: UIButton!
    
    @IBOutlet weak var docTableView: UITableView! {
        
            didSet {
                docTableView.register(UINib(nibName: Constants.homeCellNibName, bundle: nil), forCellReuseIdentifier: Constants.docInfoReuseId)
                
                docTableView.dataSource = self
                docTableView
                    .delegate = self
            }
        }
    
    
    @IBOutlet weak var uploadButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = model.name
        // Do any additional setup after loading the view.
    }
    

    @IBAction func didTapInfo(_ sender: UIButton) {
    }
    
    
    @IBAction func didTapUploadDocs(_ sender: UIButton) {
        performSegue(withIdentifier: Constants.segueue.uploadDocs , sender: self)
    }

    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let vc = segue.destination as? UploadDocViewController {
            vc.model = UploadDocsModel()
            vc.model.docNameArray = model.documentList
        }
    }


}

extension SchemeInfoViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.documentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.docInfoReuseId) as? HomeTableViewCell else {
            return UITableViewCell()
        }
        
        cell.load(data: HomeDataSource(title: "", description: model.documentList[indexPath.row], imageUrl: "pointer"))
        return cell

    }
    
    
    
    
}
