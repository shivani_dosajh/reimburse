//
//  SchemeInfoMoel.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.
//

import Foundation

struct schemeInfo {
    var name : String
    var descripiton : String
    var documentList : [String]

}

class SchemeInfoModel {
    
    var schemeData = [
        schemeInfo(name: "Central Government Health Scheme",
            descripiton: "For the last six decades Central Government Health Scheme is providing comprehensive medical care to the Central Government employees and pensioners enrolled under the scheme. In fact CGHS caters to the healthcare needs of eligible beneficiaries covering all four pillars of democratic set up in India namely Legislature, Judiciary, Executive and Press. CGHS is the model Health care facility provider for Central Government employees & Pensioners and is unique of its kind due to the large volume of beneficiary base, and open ended generous approach of providing health care.",
                                  documentList: [
                                    "Copy of CGHS card of patient",
                                    
                                    "Emergency certificate(in original), in case of emergency admission",
                                    "discharge summery Document",
                                    "Original bills/vouchers/cash memo etc. for the amount claimed"
                                    
        ]),
        schemeInfo(name:"Delhi Government Employees Health Scheme",
                   descripiton: "Delhi Government Employees Health Scheme (DGEHS) was launched in April 1997 with a view to provide comprehensive medical facilities to Delhi Government employees and pensioners and their dependants on the pattern of Central Government Health Scheme. All health facilities (hospitals/dispensaries) run by the Govt. of NCT of Delhi and autonomous bodies under Delhi Government, local bodies viz. MCD, NDMC, Delhi Cantonment Board, Central Government and other Government bodies such as AIIMS, Patel Chest Institute (University of Delhi) etc. are recognized under the scheme. In addition, some Private Hospitals/Diagnostic centers notified from time to time are also empanelled/ empanelled as referral health facilities",
                   documentList: [
                    "Covering letter/self representation by the beneficiary",
                    "Summary of medical bills claimed",
                    "Summary of medical bills claimed.",
                    "All original bills",
                    "Photocopy of Valid DGEHS Medical card",
                    "Prescription slip"
            ])
                       
                       ]
}
