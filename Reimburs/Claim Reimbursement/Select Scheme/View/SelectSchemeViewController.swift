//
//  SelectSchemeViewController.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 01/06/18.
//

import UIKit

class SelectSchemeViewController: UIViewController {
    
    var model = SelectSchemeModel()
    var rowId : Int!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var schemeTableView: UITableView! {
            didSet {
                schemeTableView.register(UINib(nibName: Constants.homeCellNibName, bundle: nil), forCellReuseIdentifier: Constants.selectChemeReuseId)
                
                schemeTableView.dataSource = self
                schemeTableView.delegate = self
            }
        }
        
    
    
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let vc = segue.destination as? SchemeInfoViewController {
            vc.model = SchemeInfoModel().schemeData[rowId]
            
        }
        
    
    }
 

}


extension SelectSchemeViewController : UITableViewDataSource ,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.schemeData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.selectChemeReuseId) as? HomeTableViewCell else {
            return UITableViewCell()
        }
        cell.load(data: model.schemeData[indexPath.row])
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        rowId = indexPath.row
        performSegue(withIdentifier: Constants.segueue.selectSchemetoInfo, sender: self)
        
       
            
        
    }
    
    
}
