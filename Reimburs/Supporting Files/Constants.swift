//
//  Constants.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 01/06/18.
//

import Foundation
import UIKit

enum Constants {
    
    static let homeCellNibName = "HomeTableViewCell"
    static let homeReuseId = "HomeTVCellReuseId"
    static let selectChemeReuseId = "SelectSchemeReuseId"
    static let docInfoReuseId = "docInfoReuseId"
    static let docUploadReuseId = "docUploadReuseId"
    static let trackStatusReuseId = "trackStatusReuseId"
    static let aadharNum = "aadharNum"
    
    enum screen {
        static let width = UIScreen.main.bounds.width
        static let height = UIScreen.main.bounds.height
    }
    
    enum segueue {
        static let homeToClaim = "homeToCLaim"
        static let homeToTracking = "homeToTracking"
        static let homeToLinkage = "homeToLinkage"
        static let selectSchemetoInfo = "selectSchemeToInfo"
        static let uploadDocs = "uploadDocs"

        
        
    }
    
    static let userId = "userId"
    
}
