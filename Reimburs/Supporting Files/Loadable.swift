//
//  Loadable.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 02/06/18.
//

import Foundation

protocol Loadable {
    func showLoader()
    func hideLoader()
    
}
