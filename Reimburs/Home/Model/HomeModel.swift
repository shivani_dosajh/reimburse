//
//  HomeModel.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 01/06/18.
//

import Foundation


struct HomeDataSource {
    let title : String!
    let description : String!
    let imageUrl : String!
}

class HomeModel {
    
    var homeData = [
        HomeDataSource(title: "Claim Reimbursement", description: "proceed to add medical bills to claim the reimbursement", imageUrl: "claim"),
        HomeDataSource(title: "Track Status", description: "Check the status of your medical claims", imageUrl: "trackStatus"),
        HomeDataSource(title: "Link Account", description: "Link your Account details for direct transmission of benefits", imageUrl: "linkAccount")
    
    ]
    
}

