//
//  TableViewCell.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 01/06/18.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImageVIew: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func load(data: HomeDataSource) {
        titleLabel.text = data.title
        descriptionLabel.text = data.description
        iconImageVIew.image = UIImage(named: data.imageUrl)
    }
    
}

