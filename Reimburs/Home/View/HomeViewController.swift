//
//  HomeViewController.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 01/06/18.
//

import UIKit

class HomeViewController: UIViewController {
    var model = HomeModel()

    @IBOutlet weak var homeTableView: UITableView! {
        didSet {
            homeTableView.register(UINib(nibName: Constants.homeCellNibName, bundle: nil), forCellReuseIdentifier: Constants.homeReuseId)
            
            homeTableView.dataSource = self
            homeTableView.delegate = self
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

  
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.homeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.homeReuseId) as? HomeTableViewCell else {
            return UITableViewCell()
        }
        cell.load(data: model.homeData[indexPath.row])
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0 : performSegue(withIdentifier: Constants.segueue.homeToClaim, sender: self)
        case 1 : performSegue(withIdentifier: Constants.segueue.homeToTracking, sender: self)
        case 2 : performSegue(withIdentifier: Constants.segueue.homeToLinkage, sender: self)
        default : return
        }
    }
    
    
}
