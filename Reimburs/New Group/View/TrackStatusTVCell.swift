//
//  TrackStatusTVCell.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 03/06/18.

//

import UIKit

class TrackStatusTVCell: UITableViewCell {

    @IBOutlet weak var statusDescriptionLabel: UILabel!
    @IBOutlet weak var claimNumDescriptionLabel: UILabel!
    @IBOutlet weak var schemeDescripitonLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
}
