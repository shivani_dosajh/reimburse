//
//  TrackStatusViewController.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 03/06/18.

//

import UIKit

class TrackStatusViewController: UIViewController {
    var model : TrackStatusModel?
    
    @IBOutlet weak var trackTableView: UITableView!{
        didSet {
            trackTableView.register(UINib(nibName: "TrackStatusTVCell", bundle: nil), forCellReuseIdentifier: Constants.trackStatusReuseId)
            trackTableView.delegate = self
            trackTableView.dataSource = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TrackStatusViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
       guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.trackStatusReuseId) as? TrackStatusTVCell else{
            return UITableViewCell()
        }
        
        return cell
        
        
    }
    
    
    
}
