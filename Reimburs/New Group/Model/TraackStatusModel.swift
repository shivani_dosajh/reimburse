//
//  TraackStatusModel.swift
//  Reimburs
//
//  Created by Shivani Dosajh on 03/06/18.

//

import Foundation
import ObjectMapper


struct claimObject : Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
    }
    
    
    var claimNumber : String?
    var claimStatus : String?
    var SchemeName : String?
    
    
    
    
}

class TrackStatusModel {
    
    var claims : [claimObject]?
}
